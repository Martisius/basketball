package com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.PlayerConverter;

import com.example.lukasmartisius.basketball.UsableObjects.JsonParsers.OtherFunctions;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONPlayer;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONPlayerList;
import com.example.lukasmartisius.basketball.Model.NeededModels.Player;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;

import java.util.ArrayList;
import java.util.List;

public class PlayersConverter implements IPlayersMapper {
    @Override
    public List<Player> mapFromDbToApp(List<PlayerDb> playerDbs) {
        ArrayList<Player> players = new ArrayList<>();
        Player player;
        for (PlayerDb p:playerDbs) {
            player = new Player(p.getPlayerName(), p.getPlayerAge(), p.getPlayerHeight(), p.getPlayerWeight(), p.getPlayerDescription(), p.getPlayerPosition(), p.getPlayerImage(), p.getPlayerID());
            players.add(player);
        }
        return players;
    }

    @Override
    public List<PlayerDb> mapFromJsonToDb(JSONPlayerList jsonPlayerList) {
            ArrayList<PlayerDb> playerDbs = new ArrayList<>();
            PlayerDb playerDb;
            if(jsonPlayerList.getPlayersList()!=null)
                for (JSONPlayer p:jsonPlayerList.getPlayersList()) {
                    playerDb = new PlayerDb();
                    playerDb.setPlayerName(p.getStrPlayer());
                    playerDb.setPlayerAge(OtherFunctions.getAgeFromBirthString(p.getDateBorn()));
                    playerDb.setPlayerHeight(OtherFunctions.getHeightFromString(p.getStrHeight()));
                    playerDb.setPlayerWeight(OtherFunctions.getWeightFromString(p.getStrWeight()));
                    playerDb.setPlayerDescription(p.getStrDescriptionEN());
                    playerDb.setPlayerImage(p.getStrThumb());
                    playerDb.setPlayerPosition(p.getStrPosition());
                    playerDb.setPlayerID(p.getID());
                    playerDb.setTeamID(p.getIdTeam());
                    playerDbs.add(playerDb);
                }
            return playerDbs;
    }

    @Override
    public Player mapPlayerFromDbToApp(PlayerDb playerDb) {
        return new Player(playerDb.getPlayerName(), playerDb.getPlayerAge(), playerDb.getPlayerHeight(),
                playerDb.getPlayerWeight(), playerDb.getPlayerDescription(), playerDb.getPlayerPosition(),
                playerDb.getPlayerImage(), playerDb.getPlayerID());
    }
}
