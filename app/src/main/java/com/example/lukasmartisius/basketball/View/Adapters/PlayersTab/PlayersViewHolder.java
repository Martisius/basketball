package com.example.lukasmartisius.basketball.View.Adapters.PlayersTab;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lukasmartisius.basketball.R;

public class PlayersViewHolder extends RecyclerView.ViewHolder{
    public TextView playerNamePosition;
    public ImageView playerImage;

    public PlayersViewHolder(@NonNull View itemView) {
        super(itemView);
        playerImage = itemView.findViewById(R.id.playerImage);
        playerNamePosition = itemView.findViewById(R.id.playerNamePosition);
    }
}
