package com.example.lukasmartisius.basketball.View.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lukasmartisius.basketball.DependencyProvider;
import com.example.lukasmartisius.basketball.Handlers.RecyclerItemClickListener;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;
import com.example.lukasmartisius.basketball.R;
import com.example.lukasmartisius.basketball.View.Activities.PlayerDetailsActivity;
import com.example.lukasmartisius.basketball.View.Adapters.PlayersTab.PlayersAdapter;
import com.example.lukasmartisius.basketball.viewmodel.TeamDetailsViewModel;

import java.util.List;

public class PlayersFragment extends Fragment {

    private final static String PLAYER_ID = "playerID";
    private View view;
    private PlayersAdapter adapter;

    public PlayersFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.players_fragment, container,false);

        adapter = new PlayersAdapter();
        ViewModelProviders.of(getActivity()).get(TeamDetailsViewModel.class).getPlayersList().observe(this, new Observer<List<PlayerDb>>() {
            @Override
            public void onChanged(@Nullable List<PlayerDb> playerDbs) {
                adapter.setPlayers(DependencyProvider.getPlayersMapper().mapFromDbToApp(playerDbs));
            }
        });

        RecyclerView rv = view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setAdapter(adapter);
        rv.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {

                        Intent intent = new Intent(v.getContext(), PlayerDetailsActivity.class);
                        //Toast.makeText(getActivity(),  teamID, Toast.LENGTH_SHORT).show();
                        intent.putExtra(PLAYER_ID, adapter.getPlayerId(position));
                        startActivity(intent);
                    }
                })
        );

        return view;
    }
}
