package com.example.lukasmartisius.basketball.Repositories.RoomRepository;

import android.arch.lifecycle.LiveData;

import com.example.lukasmartisius.basketball.Model.NeededModels.Player;
import com.example.lukasmartisius.basketball.Model.NeededModels.Team;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.EventDao;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.PlayerDao;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.TeamDao;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.PlayerConverter.IPlayersMapper;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter.ITeamMapper;
import com.example.lukasmartisius.basketball.UsableObjects.SportRoomDatabase;

import java.util.List;

public class DatabaseManagerImpl implements IDatabaseManager {
    private TeamDao teamDao;
    private PlayerDao playerDao;
    private EventDao eventDao;
    private ITeamMapper teamMapper;
    private IPlayersMapper playersMapper;

    public DatabaseManagerImpl(SportRoomDatabase sportRoomDatabase, ITeamMapper teamMapper, IPlayersMapper playersMapper) {
        this.teamMapper = teamMapper;
        this.playersMapper = playersMapper;
        teamDao = sportRoomDatabase.teamDao();
        playerDao = sportRoomDatabase.playerDao();
        eventDao = sportRoomDatabase.eventDao();
    }

    @Override
    public void updateTeams(List<TeamDb> teamDbs) {
        new RoomDatabaseManager.insertAllTeamsAsyncTask(teamDao).execute(teamDbs);
    }

    @Override
    public void updatePlayers(int teamID, List<PlayerDb> playerDbs) {
        new RoomDatabaseManager.insertAllPlayersAsyncTask(playerDao, teamID).execute(playerDbs);
    }

    @Override
    public void updateEvents(int teamID, List<EventDb> eventDbs) {
        new RoomDatabaseManager.insertAllEventsAsyncTask(eventDao, teamID).execute(eventDbs);
    }

    @Override
    public LiveData<List<TeamDb>> getTeamsList() {
        return teamDao.getAll();
    }

    @Override
    public LiveData<List<EventDb>> getEventsList(int teamID) {
        return eventDao.getAll(teamID);
    }

    @Override
    public LiveData<List<PlayerDb>> getPlayersList(int teamID) {
        return playerDao.getAll(teamID);
    }

    @Override
    public Player getPlayer(int playerID) {
        return playersMapper.mapPlayerFromDbToApp(playerDao.getPlayer(playerID));
    }

    @Override
    public Team getTeam(int teamID) {
        return teamMapper.mapTeamFromDbToApp(teamDao.getTeam(teamID));
    }
}
