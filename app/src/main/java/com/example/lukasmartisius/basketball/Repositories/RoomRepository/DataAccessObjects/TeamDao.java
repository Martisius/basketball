package com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;
import com.example.lukasmartisius.basketball.UsableObjects.DbContract;

import java.util.List;

@Dao
public interface TeamDao {
    @Query("SELECT * FROM " + DbContract.DbEntry.TEAM_TABLE_NAME)
    LiveData<List<TeamDb>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TeamDb> teams);

    @Insert
    void insert(TeamDb... team);

    @Delete
    void delete(TeamDb team);

    @Query("DELETE FROM " + DbContract.DbEntry.TEAM_TABLE_NAME)
    void cleanTable();

    @Query("SELECT * FROM " + DbContract.DbEntry.TEAM_TABLE_NAME +
    " WHERE " + DbContract.DbEntry.COLUMN_NAME_TEAM_ID + " = :teamID")
    TeamDb getTeam(int teamID);
}
