package com.example.lukasmartisius.basketball.Model.FullModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JSONTeam {

        @SerializedName("idTeam")
        String idTeam;
        @SerializedName("intLoved")
        String intLoved;
        @SerializedName("strTeam")
        String strTeam;
        @SerializedName("strTeamShort")
        String strTeamShort;
        @SerializedName("strAlternate")
        String strAlternate;
        @SerializedName("intFormedYear")
        String intFormedYear;
        @SerializedName("strSport")
        String strSport;
        @SerializedName("strLeague")
        String strLeague;
        @SerializedName("idLeague")
        String idLeague;
        @SerializedName("strDivision")
        Object strDivision;
        @SerializedName("strManager")
        String strManager;
        @SerializedName("strStadium")
        String strStadium;
        @SerializedName("strKeywords")
        String strKeywords;
        @SerializedName("strRSS")
        String strRSS;
        @SerializedName("strStadiumThumb")
        String strStadiumThumb;
        @SerializedName("strStadiumDescription")
        String strStadiumDescription;
        @SerializedName("strStadiumLocation")
        String strStadiumLocation;
        @SerializedName("intStadiumCapacity")
        String intStadiumCapacity;
        @SerializedName("strWebsite")
        String strWebsite;
        @SerializedName("strFacebook")
        String strFacebook;
        @SerializedName("strTwitter")
        String strTwitter;
        @SerializedName("strInstagram")
        String strInstagram;
        @SerializedName("strDescriptionEN")
        String strDescriptionEN;
        @SerializedName("strDescriptionDE")
        String strDescriptionDE;
        @SerializedName("strDescriptionFR")
        Object strDescriptionFR;
        @SerializedName("strDescriptionCN")
        Object strDescriptionCN;
        @SerializedName("strDescriptionIT")
        String strDescriptionIT;
        @SerializedName("strDescriptionJP")
        Object strDescriptionJP;
        @SerializedName("strDescriptionRU")
        Object strDescriptionRU;
        @SerializedName("strDescriptionES")
        Object strDescriptionES;
        @SerializedName("strDescriptionPT")
        Object strDescriptionPT;
        @SerializedName("strDescriptionSE")
        Object strDescriptionSE;
        @SerializedName("strDescriptionNL")
        Object strDescriptionNL;
        @SerializedName("strDescriptionHU")
        Object strDescriptionHU;
        @SerializedName("strDescriptionNO")
        Object strDescriptionNO;
        @SerializedName("strDescriptionIL")
        Object strDescriptionIL;
        @SerializedName("strDescriptionPL")
        Object strDescriptionPL;
        @SerializedName("strGender")
        String strGender;
        @SerializedName("strCountry")
        String strCountry;
        @SerializedName("strTeamBadge")
        String strTeamBadge;
        @SerializedName("strTeamJersey")
        String strTeamJersey;
        @SerializedName("strTeamLogo")
        String strTeamLogo;
        @SerializedName("strTeamFanart1")
        String strTeamFanart1;
        @SerializedName("strTeamFanart2")
        String strTeamFanart2;
        @SerializedName("strTeamFanart3")
        String strTeamFanart3;
        @SerializedName("strTeamFanart4")
        String strTeamFanart4;
        @SerializedName("strTeamBanner")
        String strTeamBanner;
        @SerializedName("strYoutube")
        String strYoutube;
        @SerializedName("strLocked")
        String strLocked;

        public String getIdTeam() {
            return idTeam;
        }

        public void setIdTeam(String idTeam) {
            this.idTeam = idTeam;
        }

        /*public String getIdSoccerXML() {
            return idSoccerXML;
        }

        public void setIdSoccerXML(String idSoccerXML) {
            this.idSoccerXML = idSoccerXML;
        }*/

        public String getIntLoved() {
            return intLoved;
        }

        public void setIntLoved(String intLoved) {
            this.intLoved = intLoved;
        }

        public String getStrTeam() {
            return strTeam;
        }

        public void setStrTeam(String strTeam) {
            this.strTeam = strTeam;
        }

        public String getStrTeamShort() {
            return strTeamShort;
        }

        public void setStrTeamShort(String strTeamShort) {
            this.strTeamShort = strTeamShort;
        }

        public String getStrAlternate() {
            return strAlternate;
        }

        public void setStrAlternate(String strAlternate) {
            this.strAlternate = strAlternate;
        }

        public String getIntFormedYear() {
            return intFormedYear;
        }

        public void setIntFormedYear(String intFormedYear) {
            this.intFormedYear = intFormedYear;
        }

        public String getStrSport() {
            return strSport;
        }

        public void setStrSport(String strSport) {
            this.strSport = strSport;
        }

        public String getStrLeague() {
            return strLeague;
        }

        public void setStrLeague(String strLeague) {
            this.strLeague = strLeague;
        }

        public String getIdLeague() {
            return idLeague;
        }

        public void setIdLeague(String idLeague) {
            this.idLeague = idLeague;
        }

        public Object getStrDivision() {
            return strDivision;
        }

        public void setStrDivision(Object strDivision) {
            this.strDivision = strDivision;
        }

        public String getStrManager() {
            return strManager;
        }

        public void setStrManager(String strManager) {
            this.strManager = strManager;
        }

        public String getStrStadium() {
            return strStadium;
        }

        public void setStrStadium(String strStadium) {
            this.strStadium = strStadium;
        }

        public String getStrKeywords() {
            return strKeywords;
        }

        public void setStrKeywords(String strKeywords) {
            this.strKeywords = strKeywords;
        }

        public String getStrRSS() {
            return strRSS;
        }

        public void setStrRSS(String strRSS) {
            this.strRSS = strRSS;
        }

        public String getStrStadiumThumb() {
            return strStadiumThumb;
        }

        public void setStrStadiumThumb(String strStadiumThumb) {
            this.strStadiumThumb = strStadiumThumb;
        }

        public String getStrStadiumDescription() {
            return strStadiumDescription;
        }

        public void setStrStadiumDescription(String strStadiumDescription) {
            this.strStadiumDescription = strStadiumDescription;
        }

        public String getStrStadiumLocation() {
            return strStadiumLocation;
        }

        public void setStrStadiumLocation(String strStadiumLocation) {
            this.strStadiumLocation = strStadiumLocation;
        }

        public String getIntStadiumCapacity() {
            return intStadiumCapacity;
        }

        public void setIntStadiumCapacity(String intStadiumCapacity) {
            this.intStadiumCapacity = intStadiumCapacity;
        }

        public String getStrWebsite() {
            return strWebsite;
        }

        public void setStrWebsite(String strWebsite) {
            this.strWebsite = strWebsite;
        }

        public String getStrFacebook() {
            return strFacebook;
        }

        public void setStrFacebook(String strFacebook) {
            this.strFacebook = strFacebook;
        }

        public String getStrTwitter() {
            return strTwitter;
        }

        public void setStrTwitter(String strTwitter) {
            this.strTwitter = strTwitter;
        }

        public String getStrInstagram() {
            return strInstagram;
        }

        public void setStrInstagram(String strInstagram) {
            this.strInstagram = strInstagram;
        }

        public String getStrDescriptionEN() {
            return strDescriptionEN;
        }

        public void setStrDescriptionEN(String strDescriptionEN) {
            this.strDescriptionEN = strDescriptionEN;
        }

        public String getStrDescriptionDE() {
            return strDescriptionDE;
        }

        public void setStrDescriptionDE(String strDescriptionDE) {
            this.strDescriptionDE = strDescriptionDE;
        }

        public Object getStrDescriptionFR() {
            return strDescriptionFR;
        }

        public void setStrDescriptionFR(Object strDescriptionFR) {
            this.strDescriptionFR = strDescriptionFR;
        }

        public Object getStrDescriptionCN() {
            return strDescriptionCN;
        }

        public void setStrDescriptionCN(Object strDescriptionCN) {
            this.strDescriptionCN = strDescriptionCN;
        }

        public String getStrDescriptionIT() {
            return strDescriptionIT;
        }

        public void setStrDescriptionIT(String strDescriptionIT) {
            this.strDescriptionIT = strDescriptionIT;
        }

        public Object getStrDescriptionJP() {
            return strDescriptionJP;
        }

        public void setStrDescriptionJP(Object strDescriptionJP) {
            this.strDescriptionJP = strDescriptionJP;
        }

        public Object getStrDescriptionRU() {
            return strDescriptionRU;
        }

        public void setStrDescriptionRU(Object strDescriptionRU) {
            this.strDescriptionRU = strDescriptionRU;
        }

        public Object getStrDescriptionES() {
            return strDescriptionES;
        }

        public void setStrDescriptionES(Object strDescriptionES) {
            this.strDescriptionES = strDescriptionES;
        }

        public Object getStrDescriptionPT() {
            return strDescriptionPT;
        }

        public void setStrDescriptionPT(Object strDescriptionPT) {
            this.strDescriptionPT = strDescriptionPT;
        }

        public Object getStrDescriptionSE() {
            return strDescriptionSE;
        }

        public void setStrDescriptionSE(Object strDescriptionSE) {
            this.strDescriptionSE = strDescriptionSE;
        }

        public Object getStrDescriptionNL() {
            return strDescriptionNL;
        }

        public void setStrDescriptionNL(Object strDescriptionNL) {
            this.strDescriptionNL = strDescriptionNL;
        }

        public Object getStrDescriptionHU() {
            return strDescriptionHU;
        }

        public void setStrDescriptionHU(Object strDescriptionHU) {
            this.strDescriptionHU = strDescriptionHU;
        }

        public Object getStrDescriptionNO() {
            return strDescriptionNO;
        }

        public void setStrDescriptionNO(Object strDescriptionNO) {
            this.strDescriptionNO = strDescriptionNO;
        }

        public Object getStrDescriptionIL() {
            return strDescriptionIL;
        }

        public void setStrDescriptionIL(Object strDescriptionIL) {
            this.strDescriptionIL = strDescriptionIL;
        }

        public Object getStrDescriptionPL() {
            return strDescriptionPL;
        }

        public void setStrDescriptionPL(Object strDescriptionPL) {
            this.strDescriptionPL = strDescriptionPL;
        }

        public String getStrGender() {
            return strGender;
        }

        public void setStrGender(String strGender) {
            this.strGender = strGender;
        }

        public String getStrCountry() {
            return strCountry;
        }

        public void setStrCountry(String strCountry) {
            this.strCountry = strCountry;
        }

        public String getStrTeamBadge() {
            return strTeamBadge;
        }

        public void setStrTeamBadge(String strTeamBadge) {
            this.strTeamBadge = strTeamBadge;
        }

        public String getStrTeamJersey() {
            return strTeamJersey;
        }

        public void setStrTeamJersey(String strTeamJersey) {
            this.strTeamJersey = strTeamJersey;
        }

        public String getStrTeamLogo() {
            return strTeamLogo;
        }

        public void setStrTeamLogo(String strTeamLogo) {
            this.strTeamLogo = strTeamLogo;
        }

        public String getStrTeamFanart1() {
            return strTeamFanart1;
        }

        public void setStrTeamFanart1(String strTeamFanart1) {
            this.strTeamFanart1 = strTeamFanart1;
        }

        public String getStrTeamFanart2() {
            return strTeamFanart2;
        }

        public void setStrTeamFanart2(String strTeamFanart2) {
            this.strTeamFanart2 = strTeamFanart2;
        }

        public String getStrTeamFanart3() {
            return strTeamFanart3;
        }

        public void setStrTeamFanart3(String strTeamFanart3) {
            this.strTeamFanart3 = strTeamFanart3;
        }

        public String getStrTeamFanart4() {
            return strTeamFanart4;
        }

        public void setStrTeamFanart4(String strTeamFanart4) {
            this.strTeamFanart4 = strTeamFanart4;
        }

        public String getStrTeamBanner() {
            return strTeamBanner;
        }

        public void setStrTeamBanner(String strTeamBanner) {
            this.strTeamBanner = strTeamBanner;
        }

        public String getStrYoutube() {
            return strYoutube;
        }

        public void setStrYoutube(String strYoutube) {
            this.strYoutube = strYoutube;
        }

        public String getStrLocked() {
            return strLocked;
        }

        public void setStrLocked(String strLocked) {
            this.strLocked = strLocked;
        }
}
