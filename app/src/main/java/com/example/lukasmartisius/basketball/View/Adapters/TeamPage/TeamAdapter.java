package com.example.lukasmartisius.basketball.View.Adapters.TeamPage;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lukasmartisius.basketball.Model.NeededModels.Team;
import com.example.lukasmartisius.basketball.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TeamAdapter extends RecyclerView.Adapter<TeamViewHolder>{
    private List<Team> teamList = new ArrayList<>();

    public TeamAdapter() {

    }

    @NonNull
    @Override
    public TeamViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.team_adapter, viewGroup, false);

        itemView.getLayoutParams().height = Resources.getSystem().getDisplayMetrics().heightPixels*2/7; //1/3.5 part of the screen

        return new TeamViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamViewHolder teamViewHolder, int i) {
        Team team = teamList.get(i);
        teamViewHolder.teamName.setText(team.getTeamName());
        teamViewHolder.teamDescription.setText(team.getTeamDescription());

        Picasso.get().load(team.getTeamIcon()).into(teamViewHolder.teamIcon);
    }

    public void setTeams(List<Team> teams) {
        teamList = teams;
        notifyDataSetChanged();
    }

    public int getTeamID(int position) {
        return teamList.get(position).getID();
    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }
}
