package com.example.lukasmartisius.basketball.Interfaces;

public interface Callback<T> {
    void onResult(T result);
    void onFailure(Throwable result);
}
