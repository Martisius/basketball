package com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.EventConverter;

import com.example.lukasmartisius.basketball.Model.FullModels.JSONEvent;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONEventList;
import com.example.lukasmartisius.basketball.Model.NeededModels.Event;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;

import java.util.ArrayList;
import java.util.List;

public class EventsConverter implements IEventMapper {
    @Override
    public List<Event> mapFromDbToApp(List<EventDb> eventDbs) {
        ArrayList<Event> events = new ArrayList<>();
        Event event;
        for (EventDb e:eventDbs) {
            event = new Event(e.getEventDate(), e.getHomeTeamName(), e.getAwayTeamName());
            events.add(event);
        }
        return events;
    }

    @Override
    public List<EventDb> mapFromJsonToDb(JSONEventList jsonEventList) {
        ArrayList<EventDb> eventDbs = new ArrayList<>();
        EventDb eventDb;
        if(jsonEventList.getEventList()!=null)
            for (JSONEvent e:jsonEventList.getEventList()) {
                eventDb = new EventDb();
                eventDb.setEventDate(e.getDateEvent());
                eventDb.setHomeTeamName(e.getStrHomeTeam());
                eventDb.setAwayTeamName(e.getStrAwayTeam());
                eventDb.setHomeTeamID(e.getIdHomeTeam());
                eventDb.setAwayTeamID(e.getIdAwayTeam());
                eventDbs.add(eventDb);
            }
        return eventDbs;
    }
}
