package com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter;

import com.example.lukasmartisius.basketball.Model.FullModels.JSONTeamList;
import com.example.lukasmartisius.basketball.Model.NeededModels.Team;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;

import java.util.List;

public interface ITeamMapper {
    List<Team> mapFromDbToApp(List<TeamDb> teamDbs);
    List<TeamDb> mapFromJsonToDb(JSONTeamList jsonTeamList);
    Team mapTeamFromDbToApp(TeamDb teamDb);
}
