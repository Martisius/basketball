package com.example.lukasmartisius.basketball.Repositories.RoomRepository;

import android.os.AsyncTask;

import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.EventDao;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.PlayerDao;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.TeamDao;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;

import java.util.List;

public class RoomDatabaseManager {

    public static class insertAllTeamsAsyncTask extends AsyncTask<List<TeamDb>, Void, Void> {
        private TeamDao asyncTeamDao;

        public insertAllTeamsAsyncTask(TeamDao dao) {
            asyncTeamDao = dao;
        }

        @Override
        protected Void doInBackground(final List<TeamDb>... params) {
            asyncTeamDao.cleanTable();
            asyncTeamDao.insertAll(params[0]);
            return null;
        }
    }

    public static class insertAllPlayersAsyncTask extends AsyncTask<List<PlayerDb>, Void, Void> {
        private PlayerDao asyncPlayerDao;
        private int teamID;

        public insertAllPlayersAsyncTask(PlayerDao dao, int teamID) {
            asyncPlayerDao = dao;
            this.teamID = teamID;
        }

        @Override
        protected Void doInBackground(final List<PlayerDb>... params) {
            asyncPlayerDao.cleanPlayers(teamID);
            asyncPlayerDao.insertAll(params[0]);
            return null;
        }
    }

    public static class insertAllEventsAsyncTask extends AsyncTask<List<EventDb>, Void, Void> {
        private EventDao asyncEventDao;
        private int teamID;

        public insertAllEventsAsyncTask(EventDao dao, int teamID) {
            asyncEventDao = dao;
            this.teamID = teamID;
        }

        @Override
        protected Void doInBackground(final List<EventDb>... params) {
            asyncEventDao.cleanEvents(teamID);
            asyncEventDao.insertAll(params[0]);
            return null;
        }
    }
}
