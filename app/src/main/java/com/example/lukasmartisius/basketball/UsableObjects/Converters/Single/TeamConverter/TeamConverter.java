package com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter;

import com.example.lukasmartisius.basketball.Model.FullModels.JSONTeam;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONTeamList;
import com.example.lukasmartisius.basketball.Model.NeededModels.Team;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;

import java.util.ArrayList;
import java.util.List;

public class TeamConverter implements ITeamMapper {
    @Override
    public List<Team> mapFromDbToApp(List<TeamDb> teamDbs) {
        ArrayList<Team> teams = new ArrayList<>();
        Team team;
        for (TeamDb t:teamDbs) {
            team = new Team(t.getTeamName(), t.getTeamDescription(), t.getTeamIcon(), t.getTeamImage(), t.getTeamID());
            teams.add(team);
        }
        return teams;
    }

    @Override
    public List<TeamDb> mapFromJsonToDb(JSONTeamList jsonTeamList) {
        ArrayList<TeamDb> teamDbs = new ArrayList<>();
        TeamDb teamDb;
        if(jsonTeamList.getTeamList()!=null)
            for (JSONTeam t:jsonTeamList.getTeamList()) {
                teamDb = new TeamDb();
                teamDb.setTeamName(t.getStrTeam());
                teamDb.setTeamDescription(t.getStrDescriptionEN());
                teamDb.setTeamIcon(t.getStrTeamBadge());
                teamDb.setTeamImage(t.getStrStadiumThumb());
                teamDb.setTeamID(Integer.parseInt(t.getIdTeam()));
                teamDbs.add(teamDb);
            }
        return teamDbs;
    }

    @Override
    public Team mapTeamFromDbToApp(TeamDb teamDb) {
        return new Team(teamDb.getTeamName(), teamDb.getTeamDescription(), teamDb.getTeamIcon(), teamDb.getTeamImage(), teamDb.getTeamID());
    }
}
