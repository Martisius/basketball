package com.example.lukasmartisius.basketball.Model.NeededModels;

import java.util.List;

public class Team {
    private String teamName;
    private String teamDescription;
    private String teamIcon;
    private String teamImage;
    private int ID;

    public String getTeamImage() {
        return teamImage;
    }

    public void setTeamImage(String teamImage) {
        this.teamImage = teamImage;
    }

    public Team(String teamName, String teamDescription, String teamIcon, String teamImage, int ID)
    {
        this.teamName = teamName;
        this.teamDescription = teamDescription;
        this.teamIcon = teamIcon;
        this.teamImage = teamImage;
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setTeamName(String teamName)
    {
        this.teamName = teamName;
    }

    public String getTeamName()
    {
        return teamName;
    }

    public void setTeamDescription(String teamDescription)
    {
        this.teamDescription = teamDescription;
    }

    public String getTeamDescription()
    {
        return teamDescription;
    }

    public void setTeamIcon(String teamIcon)
    {
        this.teamIcon = teamIcon;
    }

    public String getTeamIcon()
    {
        return  teamIcon;
    }
}
