package com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository;

import android.arch.lifecycle.LiveData;

import com.example.lukasmartisius.basketball.Interfaces.Callback;
import com.example.lukasmartisius.basketball.Model.NeededModels.Team;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;

import java.util.List;

public interface ITeamsManager {
    void getTeamList(Callback<LiveData<List<TeamDb>>> result);
    Team getTeam(int teamID);
}
