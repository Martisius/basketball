package com.example.lukasmartisius.basketball.UsableObjects.JsonParsers;

public class OtherFunctions {

    public static int getAgeFromBirthString(String birthDate)
    {
        int years = Integer.parseInt(birthDate.substring(0,4));
        return 2018-years;
    }

    public static String getWeightFromString(String weight) {
        int start = weight.indexOf('(');
        int end = weight.indexOf(')');
        if(start!=end)
            return weight.substring(weight.indexOf('(')+1, weight.indexOf(')'));
        else
            return weight;
    }

    public static String getHeightFromString(String height){
        int start = height.indexOf('(');
        int end = height.indexOf(')');
        if(start!=end)
            return height.substring(height.indexOf('(')+1, height.indexOf(')'));
        else
            return height;
    }
}
