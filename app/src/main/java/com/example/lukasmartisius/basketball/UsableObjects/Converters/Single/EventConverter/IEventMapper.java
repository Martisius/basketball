package com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.EventConverter;

import com.example.lukasmartisius.basketball.Model.FullModels.JSONEventList;
import com.example.lukasmartisius.basketball.Model.NeededModels.Event;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;

import java.util.List;

public interface IEventMapper {
    List<Event> mapFromDbToApp(List<EventDb> eventDbs);
    List<EventDb> mapFromJsonToDb(JSONEventList jsonEventList);
}
