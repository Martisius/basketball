package com.example.lukasmartisius.basketball.Dagger;

import com.example.lukasmartisius.basketball.View.Activities.MainActivity;
import com.example.lukasmartisius.basketball.View.Activities.PlayerDetailsActivity;
import com.example.lukasmartisius.basketball.View.Activities.TeamDetailsActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector
    abstract TeamDetailsActivity bindTeamDetailsActivity();

    @ContributesAndroidInjector
    abstract PlayerDetailsActivity bindPlayerDetailsActivity();
}
