package com.example.lukasmartisius.basketball.Model.FullModels;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JSONTeamList {

    @SerializedName("teams")
    List<JSONTeam> teamList;

    public JSONTeamList()
    {
        teamList = new ArrayList<JSONTeam>();
    }

    public static JSONTeamList parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        JSONTeamList teams = gson.fromJson(response, JSONTeamList.class);
        return teams;
    }

    public List<JSONTeam> getTeamList(){
        return teamList;
    }
}
