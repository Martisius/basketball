package com.example.lukasmartisius.basketball.RetrofitApi;

import android.arch.persistence.room.Query;

import com.example.lukasmartisius.basketball.Model.FullModels.JSONEventList;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONPlayerList;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONTeamList;

import retrofit2.Call;

public interface IRetrofitManager {
    Call<JSONTeamList> getTeamList();
    Call<JSONEventList> getEventList(int teamID);
    Call<JSONPlayerList> getPlayerList(int teamID);
}
