package com.example.lukasmartisius.basketball.Model.NeededModels;

public class Player {
    private String fullName;
    private int age;
    private String height;
    private String weight;
    private String description;
    private String playerImage;
    private String gamePosition;
    private int ID;

    public Player(String fullName, int age, String height, String weight, String description, String gamePosition, String playerImage, int ID)
    {
        this.fullName = fullName;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.description = description;
        this.gamePosition = gamePosition;
        this.playerImage = playerImage;
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getPlayerImage() {
        return playerImage;
    }

    public void setPlayerImage(String playerImage) {
        this.playerImage = playerImage;
    }

    public String getGamePosition() {
        return gamePosition;
    }

    public void setGamePosition(String gamePosition) {
        this.gamePosition = gamePosition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public int getAge() {

        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFullName() {

        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
