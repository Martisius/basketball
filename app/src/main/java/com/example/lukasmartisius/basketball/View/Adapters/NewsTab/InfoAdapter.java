package com.example.lukasmartisius.basketball.View.Adapters.NewsTab;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lukasmartisius.basketball.Model.NeededModels.Event;
import com.example.lukasmartisius.basketball.R;

import java.util.ArrayList;
import java.util.List;

public class InfoAdapter extends RecyclerView.Adapter<InfoViewHolder>{
    private List<Event> infoList = new ArrayList<>();

    public InfoAdapter() {

    }

    @NonNull
    @Override
    public InfoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.info_adapter, viewGroup, false);

        itemView.getLayoutParams().height = Resources.getSystem().getDisplayMetrics().heightPixels/9; //1/9th part of the screen

        return new InfoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InfoViewHolder infoViewHolder, int i) {
        Event info = infoList.get(i);
        infoViewHolder.dateView.setText(info.getDate().toString());
        infoViewHolder.rivalTeamName.setText(info.getbTeamName());
        infoViewHolder.myTeamName.setText(info.getaTeamName());
    }

    public void setEvents(List<Event> events) {
        this.infoList = events;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return infoList.size();
    }
}
