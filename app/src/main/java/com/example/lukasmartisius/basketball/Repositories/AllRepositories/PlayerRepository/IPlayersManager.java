package com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository;

import android.arch.lifecycle.LiveData;

import com.example.lukasmartisius.basketball.Interfaces.Callback;
import com.example.lukasmartisius.basketball.Model.NeededModels.Player;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;

import java.util.List;

public interface IPlayersManager {
    void getPlayersInTeamList(int teamID, Callback<LiveData<List<PlayerDb>>> result);
    Player getPlayer(int PlayerID);
}
