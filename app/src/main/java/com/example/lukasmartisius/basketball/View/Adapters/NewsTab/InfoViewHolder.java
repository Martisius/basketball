package com.example.lukasmartisius.basketball.View.Adapters.NewsTab;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.lukasmartisius.basketball.R;

public class InfoViewHolder extends RecyclerView.ViewHolder{

    public TextView dateView, rivalTeamName, myTeamName;

    public InfoViewHolder(@NonNull View itemView) {
        super(itemView);
        dateView = itemView.findViewById(R.id.dateView);
        rivalTeamName = itemView.findViewById(R.id.rivalTeamName);
        myTeamName = itemView.findViewById(R.id.myTeamName);
    }
}
