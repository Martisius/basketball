package com.example.lukasmartisius.basketball.RetrofitApi;

import com.example.lukasmartisius.basketball.Model.FullModels.JSONEventList;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONPlayerList;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONTeamList;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApi implements IRetrofitManager {

    Retrofit retrofit;
    APIService service;

    public RetrofitApi(String url) {
        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(APIService.class);
    }

    @Override
    public Call<JSONTeamList> getTeamList() {
        return service.getTeamList();
    }

    @Override
    public Call<JSONEventList> getEventList(int teamID) {
        return service.getEventList(teamID);
    }

    @Override
    public Call<JSONPlayerList> getPlayerList(int teamID) {
        return service.getPlayerList(teamID);
    }
}
