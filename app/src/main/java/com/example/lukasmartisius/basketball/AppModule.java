package com.example.lukasmartisius.basketball;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.Snackbar;

import com.example.lukasmartisius.basketball.Interfaces.OnResultViewListener;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.EventRepository.EventsRepository;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.EventRepository.IEventsManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository.IPlayersManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository.PlayersRepository;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository.ITeamsManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository.TeamsRepository;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DatabaseManagerImpl;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.IDatabaseManager;
import com.example.lukasmartisius.basketball.RetrofitApi.IRetrofitManager;
import com.example.lukasmartisius.basketball.RetrofitApi.RetrofitApi;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.EventConverter.EventsConverter;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.EventConverter.IEventMapper;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.PlayerConverter.IPlayersMapper;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.PlayerConverter.PlayersConverter;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter.ITeamMapper;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter.TeamConverter;
import com.example.lukasmartisius.basketball.UsableObjects.RefreshManager.IRefreshManager;
import com.example.lukasmartisius.basketball.UsableObjects.RefreshManager.RefreshManagerImpl;
import com.example.lukasmartisius.basketball.UsableObjects.SportRoomDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private static final String DATABASE_URL = "https://www.thesportsdb.com/api/v1/json/1/";

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    IRetrofitManager provideIRetrofitManager() {
        return new RetrofitApi(DATABASE_URL);
    }

    @Provides
    @Singleton
    OnResultViewListener provideOnResultViewListener() {
        return new OnResultViewListener() {
            @Override
            public void OnFailure(Throwable t) {
            }
        };
    }

    @Provides
    @Singleton
    ITeamsManager provideITeamsManager(IRefreshManager refreshManager, IRetrofitManager retrofitManager, IDatabaseManager databaseManager, ITeamMapper teamMapper) {
        return new TeamsRepository(refreshManager, retrofitManager, databaseManager, teamMapper);
    }

    @Provides
    @Singleton
    IEventsManager provideIEventManager(IRefreshManager refreshManager, IRetrofitManager retrofitManager, IDatabaseManager databaseManager, IEventMapper eventMapper) {
        return new EventsRepository(refreshManager, retrofitManager, databaseManager, eventMapper);
    }

    @Provides
    @Singleton
    IPlayersManager provideIPlayerManager(IRefreshManager refreshManager, IRetrofitManager retrofitManager, IDatabaseManager databaseManager, IPlayersMapper playersMapper) {
        return new PlayersRepository(refreshManager, retrofitManager, databaseManager, playersMapper);
    }

    @Provides
    @Singleton
    IRefreshManager provideIRefreshManager(SharedPreferences prefs) {
        return new RefreshManagerImpl(prefs);
    }

    @Provides
    @Singleton
    IDatabaseManager provideIDatabaseManager(SportRoomDatabase sportRoomDatabase, ITeamMapper teamMapper, IPlayersMapper playersMapper) {
        return new DatabaseManagerImpl(sportRoomDatabase, teamMapper, playersMapper);
    }

    @Provides
    @Singleton
    ITeamMapper provideITeamMapper() {
        return new TeamConverter();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(context.getString(R.string.preferences), Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    SportRoomDatabase provideSportRoomDatabase(Context context) {
        return SportRoomDatabase.getDatabase(context);
    }

    @Provides
    @Singleton
    IPlayersMapper provideIPlayerMapper() {
        return new PlayersConverter();
    }

    @Provides
    @Singleton
    IEventMapper provideIEventMapper() {
        return new EventsConverter();
    }
}
