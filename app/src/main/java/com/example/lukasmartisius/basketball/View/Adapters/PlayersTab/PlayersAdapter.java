package com.example.lukasmartisius.basketball.View.Adapters.PlayersTab;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lukasmartisius.basketball.Model.NeededModels.Player;
import com.example.lukasmartisius.basketball.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PlayersAdapter extends RecyclerView.Adapter<PlayersViewHolder>{
    private List<Player> playerList = new ArrayList<>();

    public PlayersAdapter() {

    }

    @NonNull
    @Override
    public PlayersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.players_adapter, viewGroup, false);
        itemView.getLayoutParams().height = Resources.getSystem().getDisplayMetrics().heightPixels/8; //1/8th part of the screen

        return new PlayersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayersViewHolder playersViewHolder, int i) {
        Player player = playerList.get(i);
        playersViewHolder.playerNamePosition.setText(player.getFullName() + ", " + player.getGamePosition());

        Picasso.get().load(player.getPlayerImage()).into(playersViewHolder.playerImage);
    }

    public void setPlayers(List<Player> players) {
        playerList = players;
        notifyDataSetChanged();
    }

    public int getPlayerId(int position) {
        return playerList.get(position).getID();
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }
}
