package com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository;

import android.arch.lifecycle.LiveData;

import com.example.lukasmartisius.basketball.Interfaces.Callback;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONPlayerList;
import com.example.lukasmartisius.basketball.Model.NeededModels.Player;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.IDatabaseManager;
import com.example.lukasmartisius.basketball.RetrofitApi.IRetrofitManager;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.PlayerConverter.IPlayersMapper;
import com.example.lukasmartisius.basketball.UsableObjects.RefreshManager.IRefreshManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class PlayersRepository implements IPlayersManager {

    private IRefreshManager refreshManager;
    private IRetrofitManager retrofitApi;
    private IDatabaseManager database;
    private IPlayersMapper playersMapper;

    public PlayersRepository(IRefreshManager refreshManager, IRetrofitManager retrofitApi, IDatabaseManager database, IPlayersMapper playersMapper) {
        this.refreshManager = refreshManager;
        this.retrofitApi = retrofitApi;
        this.database = database;
        this.playersMapper = playersMapper;
    }
    @Override
    public void getPlayersInTeamList(final int teamID, final Callback<LiveData<List<PlayerDb>>> result) {
        if(refreshManager.shouldRefresh(3, teamID)) {
            Call<JSONPlayerList> repos = retrofitApi.getPlayerList(teamID);

            repos.enqueue(new retrofit2.Callback<JSONPlayerList>() {
                @Override
                public void onResponse(Call<JSONPlayerList> call, Response<JSONPlayerList> response) {
                    JSONPlayerList jsonPlayerList = response.body();
                    List<PlayerDb> playerDbs = playersMapper.mapFromJsonToDb(jsonPlayerList);
                    database.updatePlayers(teamID, playerDbs);
                    refreshManager.refreshDate(3, teamID);
                    result.onResult(database.getPlayersList(teamID));
                }

                @Override
                public void onFailure(Call<JSONPlayerList> call, Throwable t) {
                    result.onFailure(t);
                }
            });
        } else {
            result.onResult(database.getPlayersList(teamID));
        }
    }
    @Override
    public Player getPlayer(int playerID) {
        return database.getPlayer(playerID);
    }
}
