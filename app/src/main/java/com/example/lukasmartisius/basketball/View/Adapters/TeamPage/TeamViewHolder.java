package com.example.lukasmartisius.basketball.View.Adapters.TeamPage;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lukasmartisius.basketball.R;

public class TeamViewHolder extends RecyclerView.ViewHolder{

    public ImageView teamIcon;
    public TextView teamName, teamDescription;

    public TeamViewHolder(@NonNull View itemView) {
        super(itemView);
        teamName = itemView.findViewById(R.id.teamName);
        teamDescription = itemView.findViewById(R.id.teamDescription);
        teamIcon = itemView.findViewById(R.id.teamIcon);
    }
}
