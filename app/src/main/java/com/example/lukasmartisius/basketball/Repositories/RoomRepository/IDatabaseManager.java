package com.example.lukasmartisius.basketball.Repositories.RoomRepository;

import android.arch.lifecycle.LiveData;

import com.example.lukasmartisius.basketball.Model.NeededModels.Event;
import com.example.lukasmartisius.basketball.Model.NeededModels.Player;
import com.example.lukasmartisius.basketball.Model.NeededModels.Team;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;

import java.util.List;

public interface IDatabaseManager {
    void updateTeams(List<TeamDb> teamDbs);
    void updatePlayers(int teamID, List<PlayerDb> playerDbs);
    void updateEvents(int teamID, List<EventDb> eventDbs);

    LiveData<List<TeamDb>> getTeamsList();
    LiveData<List<EventDb>> getEventsList(int teamID);
    LiveData<List<PlayerDb>> getPlayersList(int teamID);

    Player getPlayer(int playerID);
    Team getTeam(int teamID);
}
