package com.example.lukasmartisius.basketball.Enums;

public enum UpdateTime {
    TEAM(1, 60*60*1000), EVENT(2, 15*60*1000), PLAYER(3, 30*60*1000);

    private final int renewalRate;
    private int ID;
    UpdateTime(int ID, int renewalRate){
        this.renewalRate = renewalRate;
        this.ID = ID;
    }
    public int getRenewalRate(){
        return renewalRate;
    }

    public int getID(){
        return ID;
    }

    public static UpdateTime getById(int ID){
        for(UpdateTime u: values()){
            if(u.ID == ID) return u;
        }
        return null;
    }
}
