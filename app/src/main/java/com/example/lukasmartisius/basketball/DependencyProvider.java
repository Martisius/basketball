package com.example.lukasmartisius.basketball;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.lukasmartisius.basketball.Repositories.AllRepositories.EventRepository.EventsRepository;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.EventRepository.IEventsManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository.IPlayersManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository.PlayersRepository;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository.ITeamsManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository.TeamsRepository;
import com.example.lukasmartisius.basketball.RetrofitApi.IRetrofitManager;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.IDatabaseManager;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.EventConverter.EventsConverter;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.EventConverter.IEventMapper;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.PlayerConverter.IPlayersMapper;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.PlayerConverter.PlayersConverter;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter.ITeamMapper;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter.TeamConverter;
import com.example.lukasmartisius.basketball.UsableObjects.RefreshManager.IRefreshManager;
import com.example.lukasmartisius.basketball.UsableObjects.RefreshManager.RefreshManagerImpl;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DatabaseManagerImpl;
import com.example.lukasmartisius.basketball.UsableObjects.SportRoomDatabase;
import com.example.lukasmartisius.basketball.RetrofitApi.RetrofitApi;

public class DependencyProvider {
    private static DependencyProvider instance;
    private static Context context;
    private static final String PREF_NAME = "MyPref";
    private static SharedPreferences pref;

    //SINGLETONS
    private static IRetrofitManager retrofitApi;
    private static SportRoomDatabase sportRoomDatabase;
    private static IRefreshManager refreshManager;
    private static IDatabaseManager roomDatabase;

    //REPOSITORIES
    private static ITeamsManager teamsManager;
    private static IEventsManager eventsManager;
    private static IPlayersManager playersManager;

    //CONVERTERS/MAPPERS
    private static IEventMapper eventMapper;
    private static ITeamMapper teamMapper;
    private static IPlayersMapper playersMapper;


    private DependencyProvider() {
    }

    private DependencyProvider(Context context){
        this.context = context;
    }

    public static void init(Context context){
        if(instance == null) {
            synchronized (DependencyProvider.class) {
                if(instance == null)
                    instance = new DependencyProvider(context);
            }
        }
    }

    public static IDatabaseManager getRoomDatabase() {
        if(roomDatabase == null) {
            synchronized  (IDatabaseManager.class) {
                if(roomDatabase == null) {
                    roomDatabase = new DatabaseManagerImpl(getSportRoomDatabase(), getTeamMapper(), getPlayersMapper());
                }
            }
        }
        return roomDatabase;
    }

    public static IRefreshManager getRefreshManager() {
        if(refreshManager == null) {
            synchronized  (IRefreshManager.class) {
                if(refreshManager == null) {
                    refreshManager = new RefreshManagerImpl(getSharedPreferences());
                }
            }
        }
        return refreshManager;
    }

    public static SportRoomDatabase getSportRoomDatabase() {
        if(sportRoomDatabase == null) {
            synchronized  (SportRoomDatabase.class) {
                if(sportRoomDatabase == null) {
                    sportRoomDatabase = SportRoomDatabase.getDatabase(context);
                }
            }
        }
        return sportRoomDatabase;
    }

    public static IRetrofitManager getRetrofitApi() {
        if(retrofitApi == null) {
            synchronized  (RetrofitApi.class) {
                if(retrofitApi == null) {
                    retrofitApi = new RetrofitApi(context.getString(R.string.database_url));
                }
            }
        }
        return retrofitApi;
    }

    public static SharedPreferences getSharedPreferences() {
        if(pref == null){
            synchronized (SharedPreferences.class) {
                if(pref == null)
                    pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            }
        }
        return pref;
    }

    public static ITeamMapper getTeamMapper() {
        if(teamMapper == null) {
            synchronized  (ITeamMapper.class) {
                if(teamMapper == null) {
                    teamMapper = new TeamConverter();
                }
            }
        }
        return teamMapper;
    }

    public static IPlayersMapper getPlayersMapper() {
        if(playersMapper == null) {
            synchronized  (IPlayersMapper.class) {
                if(playersMapper == null) {
                    playersMapper = new PlayersConverter();
                }
            }
        }
        return playersMapper;
    }

    public static IEventMapper getEventMapper() {
        if(eventMapper == null) {
            synchronized  (IEventMapper.class) {
                if(eventMapper == null) {
                    eventMapper = new EventsConverter();
                }
            }
        }
        return eventMapper;
    }

    public static ITeamsManager getTeamsManager() {
        if(teamsManager == null) {
            synchronized  (ITeamsManager.class) {
                if(teamsManager == null) {
                    teamsManager = new TeamsRepository(getRefreshManager(), getRetrofitApi(), getRoomDatabase(), getTeamMapper());
                }
            }
        }
        return teamsManager;
    }

    public static IEventsManager getEventsManager() {
        if(eventsManager == null) {
            synchronized  (IEventsManager.class) {
                if(eventsManager == null) {
                    eventsManager = new EventsRepository(getRefreshManager(), getRetrofitApi(), getRoomDatabase(), getEventMapper());
                }
            }
        }
        return eventsManager;
    }

    public static IPlayersManager getPlayersManager() {
        if(playersManager == null) {
            synchronized  (IPlayersManager.class) {
                if(playersManager == null) {
                    playersManager = new PlayersRepository(getRefreshManager(), getRetrofitApi(), getRoomDatabase(), getPlayersMapper());
                }
            }
        }
        return playersManager;
    }

}
