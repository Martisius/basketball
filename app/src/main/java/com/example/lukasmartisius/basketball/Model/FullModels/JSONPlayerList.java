package com.example.lukasmartisius.basketball.Model.FullModels;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JSONPlayerList {
    @SerializedName("player")
    List<JSONPlayer> playersList;

    public JSONPlayerList()
    {
        playersList = new ArrayList<JSONPlayer>();
    }

    public static JSONPlayerList parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        JSONPlayerList players = gson.fromJson(response, JSONPlayerList.class);
        return players;
    }

    public List<JSONPlayer> getPlayersList(){
        return playersList;
    }
}
