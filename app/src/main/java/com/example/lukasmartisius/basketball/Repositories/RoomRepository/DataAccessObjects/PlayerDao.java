package com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;
import com.example.lukasmartisius.basketball.UsableObjects.DbContract;

import java.util.List;

@Dao
public interface PlayerDao {
    @Query("SELECT * FROM " + DbContract.DbEntry.PLAYER_TABLE_NAME + " WHERE "
            + DbContract.DbEntry.COLUMN_NAME_TEAM_ID + " = :teamID")
    LiveData<List<PlayerDb>> getAll(int teamID);

    @Query("DELETE FROM " + DbContract.DbEntry.PLAYER_TABLE_NAME + " WHERE "
            + DbContract.DbEntry.COLUMN_NAME_TEAM_ID + " = :teamID")
    void cleanPlayers(int teamID);

    @Query("SELECT * FROM " + DbContract.DbEntry.PLAYER_TABLE_NAME +
            " WHERE " + DbContract.DbEntry.COLUMN_NAME_PLAYER_ID + " = :playerID")
    PlayerDb getPlayer(int playerID);

    @Insert
    void insertAll(List<PlayerDb> playerDbs);

    @Insert
    void insert(PlayerDb... players);

    @Delete
    void delete(PlayerDb playerDb);
}
