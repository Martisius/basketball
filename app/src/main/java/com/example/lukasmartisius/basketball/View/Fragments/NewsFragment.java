package com.example.lukasmartisius.basketball.View.Fragments;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.lukasmartisius.basketball.AppModule;
import com.example.lukasmartisius.basketball.Dagger.AppComponent;
import com.example.lukasmartisius.basketball.DependencyProvider;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;
import com.example.lukasmartisius.basketball.R;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.EventConverter.IEventMapper;
import com.example.lukasmartisius.basketball.View.Adapters.NewsTab.InfoAdapter;
import com.example.lukasmartisius.basketball.viewmodel.TeamDetailsViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.DaggerActivity;
import dagger.android.DaggerFragment;

public class NewsFragment extends Fragment {

    @Inject
    TeamDetailsViewModel teamDetailsViewModel;

    @Inject
    IEventMapper eventMapper;

    @Inject
    Context context;

    private View view;
    private InfoAdapter adapter;

    public NewsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.news_fragment, container,false);

        adapter = new InfoAdapter();

        /*teamDetailsViewModel.getEventList().observe(, new Observer<List<EventDb>>() {
            @Override
            public void onChanged(@Nullable List<EventDb> eventDbs) {
                adapter.setEvents(DependencyProvider.getEventMapper().mapFromDbToApp(eventDbs));
            }
        });*/

        ViewModelProviders.of(getActivity()).get(TeamDetailsViewModel.class).getEventList().observe(this, new Observer<List<EventDb>>() {
            @Override
            public void onChanged(@Nullable List<EventDb> eventDbs) {
                adapter.setEvents(DependencyProvider.getEventMapper().mapFromDbToApp(eventDbs));
            }
        });

        RecyclerView rv = view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setAdapter(adapter);

        return view;
    }

}
