package com.example.lukasmartisius.basketball.UsableObjects;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.EventDao;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.PlayerDao;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects.TeamDao;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;


@Database(entities = {TeamDb.class, PlayerDb.class, EventDb.class}, version = 1, exportSchema = false)
public abstract class SportRoomDatabase extends RoomDatabase {
    public abstract TeamDao teamDao();
    public abstract EventDao eventDao();
    public abstract PlayerDao playerDao();

    private static volatile SportRoomDatabase INSTANCE;

    public static SportRoomDatabase getDatabase(final Context context) {
        if(INSTANCE == null) {
            synchronized (SportRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SportRoomDatabase.class, "sport_database.db")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public static SportRoomDatabase getDatabase() {
        return INSTANCE;
    }
}
