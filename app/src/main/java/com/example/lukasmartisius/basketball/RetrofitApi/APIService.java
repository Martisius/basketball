package com.example.lukasmartisius.basketball.RetrofitApi;

import com.example.lukasmartisius.basketball.Model.FullModels.JSONEventList;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONPlayerList;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONTeamList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {
    @GET("lookup_all_teams.php?id=4387")
    Call<JSONTeamList> getTeamList();

    @GET("eventsnext.php")
    Call<JSONEventList> getEventList(@Query("id") int teamID);

    @GET("lookup_all_players.php")
    Call<JSONPlayerList> getPlayerList(@Query("id") int teamID);
}
