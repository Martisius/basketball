package com.example.lukasmartisius.basketball.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.example.lukasmartisius.basketball.Interfaces.OnResultViewListener;
import com.example.lukasmartisius.basketball.Model.NeededModels.Player;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository.IPlayersManager;
import com.squareup.picasso.Picasso;

public class PlayerViewModel extends BaseObservable {
    IPlayersManager playersManager;
    Player player;

    public PlayerViewModel(final int playerId, IPlayersManager playersManager) {
        this.playersManager = playersManager;
        player = playersManager.getPlayer(playerId);
    }

    @Bindable
    public String getPlayerName(){
        return player.getFullName();
    }

    @Bindable
    public String getPlayerAge(){
        return "Age\n" + Integer.toString(player.getAge());
    }

    @Bindable
    public String getPlayerWeight(){
        return "Weight\n" + player.getWeight();
    }

    @Bindable
    public String getPlayerHeight(){
        return "Height\n" + player.getHeight();
    }

    @Bindable
    public String getPlayerDescription(){
        return player.getDescription();
    }

    @BindingAdapter("imageResource")
    public static void setPlayerImage(ImageView view, String imageUrl){
        Picasso.get().load(imageUrl).into(view);
    }

    @Bindable
    public String getPlayerImage() {
        return player.getPlayerImage();
    }
}
