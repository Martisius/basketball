package com.example.lukasmartisius.basketball.UsableObjects;

public class LastUpdate {

    private int teamID;
    private Long playerUpdateTimeStamp;
    private Long teamNewsUpdateStamp;
    private Long teamUpdateTimeStamp;

    public LastUpdate(int teamID, Long playerUpdateTimeStamp, Long teamNewsUpdateStamp, Long teamUpdateTimeStamp){
        this.teamID = teamID;
        this.playerUpdateTimeStamp = playerUpdateTimeStamp;
        this.teamNewsUpdateStamp = teamNewsUpdateStamp;
        this.teamUpdateTimeStamp = teamUpdateTimeStamp;
    }

    public Long getTeamUpdateTimeStamp() {
        return teamUpdateTimeStamp;
    }

    public void setTeamUpdateTimeStamp(Long teamUpdateTimeStamp) {
        this.teamUpdateTimeStamp = teamUpdateTimeStamp;
    }

    public int getTeamID() {
        return teamID;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    public Long getTeamNewsUpdateStamp() {
        return teamNewsUpdateStamp;
    }

    public void setTeamNewsUpdateStamp(Long teamNewsUpdateStamp) {
        this.teamNewsUpdateStamp = teamNewsUpdateStamp;
    }

    public Long getPlayerUpdateTimeStamp() {
        return playerUpdateTimeStamp;
    }

    public void setPlayerUpdateTimeStamp(Long playerUpdateTimeStamp) {
        this.playerUpdateTimeStamp = playerUpdateTimeStamp;
    }
}
