package com.example.lukasmartisius.basketball.Model.RoomModels;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.lukasmartisius.basketball.UsableObjects.DbContract;


@Entity(tableName = DbContract.DbEntry.EVENT_TABLE_NAME)
public class EventDb {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_EVENT_LOCAL_ID)
    private int localID;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_EVENT_DATE)
    private String eventDate;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_HOME_TEAM)
    private String homeTeamName;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_AWAY_TEAM)
    private String awayTeamName;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_HOME_TEAM_ID)
    private int homeTeamID;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_AWAY_TEAM_ID)
    private int awayTeamID;

    public int getLocalID() {
        return localID;
    }

    public void setLocalID(int localID) {
        this.localID = localID;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    public int getHomeTeamID() {
        return homeTeamID;
    }

    public void setHomeTeamID(int homeTeamID) {
        this.homeTeamID = homeTeamID;
    }

    public int getAwayTeamID() {
        return awayTeamID;
    }

    public void setAwayTeamID(int awayTeamID) {
        this.awayTeamID = awayTeamID;
    }
}
