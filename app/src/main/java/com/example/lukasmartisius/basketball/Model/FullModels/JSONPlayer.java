package com.example.lukasmartisius.basketball.Model.FullModels;

import com.google.gson.annotations.SerializedName;

public class JSONPlayer {
    public String getStrPlayer() {
        return strPlayer;
    }

    public void setStrPlayer(String strPlayer) {
        this.strPlayer = strPlayer;
    }

    @SerializedName("strPlayer")
    String strPlayer;

    public String getDateBorn() {
        return dateBorn;
    }

    public void setDateBorn(String dateBorn) {
        this.dateBorn = dateBorn;
    }

    @SerializedName("dateBorn")
    String dateBorn;

    public String getStrHeight() {
        return strHeight;
    }

    public void setStrHeight(String strHeight) {
        this.strHeight = strHeight;
    }

    @SerializedName("strHeight")
    String strHeight;

    public String getStrWeight() {
        return strWeight;
    }

    public void setStrWeight(String strWeight) {
        this.strWeight = strWeight;
    }

    @SerializedName("strWeight")
    String strWeight;

    public String getStrDescriptionEN() {
        return strDescriptionEN;
    }

    public void setStrDescriptionEN(String strDescriptionEN) {
        this.strDescriptionEN = strDescriptionEN;
    }

    @SerializedName("strDescriptionEN")
    String strDescriptionEN;

    public String getStrThumb() {
        return strThumb;
    }

    public void setStrThumb(String strThumb) {
        this.strThumb = strThumb;
    }

    @SerializedName("strThumb")
    String strThumb;

    public String getStrPosition() {
        return strPosition;
    }

    public void setStrPosition(String strPosition) {
        this.strPosition = strPosition;
    }

    @SerializedName("strPosition")
    String strPosition;

    public int getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(int idTeam) {
        this.idTeam = idTeam;
    }

    @SerializedName("idTeam")
    int idTeam;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @SerializedName("idPlayer")
    int ID;


}
