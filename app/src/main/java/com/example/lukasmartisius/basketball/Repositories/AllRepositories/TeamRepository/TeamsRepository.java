package com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository;

import android.arch.lifecycle.LiveData;

import com.example.lukasmartisius.basketball.Interfaces.Callback;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONTeamList;
import com.example.lukasmartisius.basketball.Model.NeededModels.Team;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.IDatabaseManager;
import com.example.lukasmartisius.basketball.RetrofitApi.IRetrofitManager;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter.ITeamMapper;
import com.example.lukasmartisius.basketball.UsableObjects.RefreshManager.IRefreshManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class TeamsRepository implements ITeamsManager {

    private IRefreshManager refreshManager;
    private IRetrofitManager retrofitApi;
    private IDatabaseManager database;
    private ITeamMapper teamMapper;

    public TeamsRepository(IRefreshManager refreshManager, IRetrofitManager retrofitApi, IDatabaseManager database, ITeamMapper teamMapper) {
        this.refreshManager = refreshManager;
        this.retrofitApi = retrofitApi;
        this.database = database;
        this.teamMapper = teamMapper;
    }

    @Override
    public void getTeamList(final Callback<LiveData<List<TeamDb>>> result) {
        if(refreshManager.shouldRefresh(1, 0)) {
            Call<JSONTeamList> repos = retrofitApi.getTeamList();

            repos.enqueue(new retrofit2.Callback<JSONTeamList>() {
                @Override
                public void onResponse(Call<JSONTeamList> call, Response<JSONTeamList> response) {
                    JSONTeamList jsonTeamList = response.body();
                    List<TeamDb> teamDbs = teamMapper.mapFromJsonToDb(jsonTeamList);
                    database.updateTeams(teamDbs);
                    refreshManager.refreshDate(1, 0);
                    result.onResult(database.getTeamsList());
                }

                @Override
                public void onFailure(Call<JSONTeamList> call, Throwable t) {
                    result.onFailure(t);
                }
            });
        } else {
            result.onResult(database.getTeamsList());
        }
    }

    @Override
    public Team getTeam(int teamID) {
        return database.getTeam(teamID);
    }
}
