package com.example.lukasmartisius.basketball.View.Activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lukasmartisius.basketball.Dagger.DaggerActivity;
import com.example.lukasmartisius.basketball.Interfaces.OnResultViewListener;
import com.example.lukasmartisius.basketball.R;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.EventRepository.IEventsManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository.IPlayersManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository.ITeamsManager;
import com.example.lukasmartisius.basketball.View.Adapters.ViewPagerAdapter;
import com.example.lukasmartisius.basketball.View.Fragments.NewsFragment;
import com.example.lukasmartisius.basketball.View.Fragments.PlayersFragment;
import com.example.lukasmartisius.basketball.viewmodel.TeamDetailsViewModel;
import com.example.lukasmartisius.basketball.databinding.ActivityTeamDetailsBinding;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class TeamDetailsActivity extends DaggerActivity implements OnResultViewListener {

    @Inject
    ITeamsManager teamsManager;

    @Inject
    IEventsManager eventsManager;

    @Inject
    IPlayersManager playersManager;

    private static final String TEAM_ID = "teamID";
    private ActivityTeamDetailsBinding binding;
    private TeamDetailsViewModel teamDetailsViewModel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_team_details);

        final View loadingView = LayoutInflater.from(this).inflate(R.layout.loading_fragment, null);
        final LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.team_details_layout, null);

        Intent intent = getIntent();
        int position = intent.getIntExtra(TEAM_ID, 0);
        teamDetailsViewModel = ViewModelProviders.of(this).get(TeamDetailsViewModel.class);
        teamDetailsViewModel.init(this, teamsManager, eventsManager, playersManager);
        binding.container.addView(loadingView);
        teamDetailsViewModel.isLoading.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if(!teamDetailsViewModel.isLoading.get()) {
                    setupList(linearLayout);
                    binding.container.removeAllViews();
                    binding.container.addView(linearLayout);
                } else {
                    binding.container.removeAllViews();
                    binding.container.addView(loadingView);
                }
            }
        });
        teamDetailsViewModel.loadLists(position);
    }

    public void setupList(LinearLayout linearLayout)
    {
        TextView teamName = linearLayout.findViewById(R.id.teamName);
        ViewPager viewPager = linearLayout.findViewById(R.id.viewPager);
        TabLayout tabLayout = linearLayout.findViewById(R.id.tabLayout);
        ImageView backdrop = linearLayout.findViewById(R.id.backdrop);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


        NewsFragment nf = new NewsFragment();
        adapter.AddFragment(nf, getString(R.string.new_tab_title));


        PlayersFragment pf = new PlayersFragment();
        adapter.AddFragment(pf, getString(R.string.player_tab_title));


        teamName.setText(teamDetailsViewModel.getTeamName());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        Picasso.get().load(teamDetailsViewModel.getTeamImage()).into(backdrop);
    }

    public void onBackClick(View v)
    {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void OnFailure(Throwable t) {
        Snackbar.make(findViewById(android.R.id.content), "Loading failed", Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.RED)
                .show();
    }
}
