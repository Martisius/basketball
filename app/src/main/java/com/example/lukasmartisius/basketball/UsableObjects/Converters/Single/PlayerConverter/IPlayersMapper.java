package com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.PlayerConverter;

import com.example.lukasmartisius.basketball.Model.FullModels.JSONPlayerList;
import com.example.lukasmartisius.basketball.Model.NeededModels.Player;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;

import java.util.List;

public interface IPlayersMapper {
    List<Player> mapFromDbToApp(List<PlayerDb> playerDbs);
    List<PlayerDb> mapFromJsonToDb(JSONPlayerList jsonPlayerList);
    Player mapPlayerFromDbToApp(PlayerDb playerDb);
}
