package com.example.lukasmartisius.basketball.View.Activities;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;

import com.example.lukasmartisius.basketball.Dagger.DaggerActivity;
import com.example.lukasmartisius.basketball.Handlers.RecyclerItemClickListener;
import com.example.lukasmartisius.basketball.Interfaces.OnResultViewListener;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;
import com.example.lukasmartisius.basketball.R;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository.ITeamsManager;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.TeamConverter.ITeamMapper;
import com.example.lukasmartisius.basketball.View.Adapters.TeamPage.TeamAdapter;
import com.example.lukasmartisius.basketball.databinding.ActivityMainBinding;
import com.example.lukasmartisius.basketball.viewmodel.TeamViewModel;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends DaggerActivity implements OnResultViewListener {

    @Inject
    ITeamsManager teamsManager;

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    ITeamMapper teamMapper;

    private static final String TEAM_ID = "teamID";
    private TeamAdapter adapter;
    private ActivityMainBinding binding;
    private TeamViewModel teamViewModel;
    private boolean setupDone = false;
    private LifecycleOwner lifecycleOwner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        lifecycleOwner = this;


        final RecyclerView listView = (RecyclerView) LayoutInflater.from(this).inflate(R.layout.teams_recycleview, null);
        final View loadingView = LayoutInflater.from(this).inflate(R.layout.loading_fragment, null);
        binding.container.addView(loadingView);

        teamViewModel = new TeamViewModel(this, teamsManager);

        teamViewModel.isLoading.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (!teamViewModel.isLoading.get()) {
                    if(!setupDone)
                        setupList(listView);
                    binding.container.removeAllViews();
                    binding.container.addView(listView);
                } else {
                    binding.container.removeAllViews();
                    binding.container.addView(loadingView);
                }
            }
        });

        teamViewModel.loadTeamList();

        binding.swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.remove(Integer.toString(1));
                        editor.putLong(Integer.toString(1), 0L);
                        editor.apply();
                        teamViewModel.loadTeamList();
                        adapter.notifyDataSetChanged();
                        binding.swiperefresh.setRefreshing(false);
                    }
                }
        );
    }

    public void setupList(RecyclerView recyclerView) {

        adapter = new TeamAdapter();
        teamViewModel.getTeamList().observe( lifecycleOwner, new Observer<List<TeamDb>>() {
            @Override
            public void onChanged(@Nullable List<TeamDb> teamDbs) {
                adapter.setTeams(teamMapper.mapFromDbToApp(teamDbs));
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.addItemDecoration(new LinearSpacingItemDecoration(dpToPx(20), dpToPx(20)));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {

                        Intent intent = new Intent(v.getContext(), TeamDetailsActivity.class);
                        //Toast.makeText(v.getContext(), Integer.toString(v.getId()), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(v.getContext(), teamViewModel.getTeamNameWithID(Integer.parseInt(v.getTag().toString())), Toast.LENGTH_SHORT).show();
                        intent.putExtra(TEAM_ID, adapter.getTeamID(position));
                        startActivity(intent);
                    }
                })
        );
        setupDone = true;
    }

    @Override
    public void OnFailure(Throwable t) {
        Snackbar.make(findViewById(android.R.id.content), "Loading failed", Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.RED)
                .show();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class LinearSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spacingWidth;
        private int spacingHeight;

        public LinearSpacingItemDecoration(int spacingWidth, int spacingHeight) {
            this.spacingWidth = spacingWidth;
            this.spacingHeight = spacingHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.left = spacingWidth;
                outRect.right = spacingWidth;
                outRect.top = spacingHeight;
                outRect.bottom = spacingHeight;
        }
    }



    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
