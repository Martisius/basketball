package com.example.lukasmartisius.basketball.Model.RoomModels;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.lukasmartisius.basketball.UsableObjects.DbContract;

@Entity(tableName = DbContract.DbEntry.TEAM_TABLE_NAME)
public class TeamDb {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_TEAM_LOCAL_ID)
    private int localID;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_TEAM_NAME)
    private String teamName;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_TEAM_DESCRIPTION)
    private String teamDescription;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_TEAM_ICON)
    private String teamIcon;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_TEAM_IMAGE)
    private String teamImage;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_TEAM_ID)
    private int teamID;

    public int getLocalID() {
        return localID;
    }

    public void setLocalID(int localID) {
        this.localID = localID;
    }

    public String getTeamImage() {
        return teamImage;
    }

    public void setTeamImage(String teamImage) {
        this.teamImage = teamImage;
    }

    public String getTeamIcon() {

        return teamIcon;
    }

    public void setTeamIcon(String teamIcon) {
        this.teamIcon = teamIcon;
    }

    public String getTeamDescription() {

        return teamDescription;
    }

    public void setTeamDescription(String teamDescription) {
        this.teamDescription = teamDescription;
    }

    public String getTeamName() {

        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getTeamID() {

        return teamID;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }
}
