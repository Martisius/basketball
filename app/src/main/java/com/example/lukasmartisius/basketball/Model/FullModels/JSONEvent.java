package com.example.lukasmartisius.basketball.Model.FullModels;

import com.google.gson.annotations.SerializedName;

public class JSONEvent {
    @SerializedName("strHomeTeam")
    String strHomeTeam;
    @SerializedName("dateEvent")
    String dateEvent;
    @SerializedName("strAwayTeam")
    String strAwayTeam;
    @SerializedName("idHomeTeam")
    int idHomeTeam;
    @SerializedName("idAwayTeam")
    int idAwayTeam;

    public int getIdAwayTeam() {
        return idAwayTeam;
    }

    public void setIdAwayTeam(int idAwayTeam) {
        this.idAwayTeam = idAwayTeam;
    }

    public int getIdHomeTeam() {
        return idHomeTeam;
    }

    public void setIdHomeTeam(int idHomeTeam) {
        this.idHomeTeam = idHomeTeam;
    }

    public String getStrAwayTeam() {
        return strAwayTeam;
    }

    public void setStrAwayTeam(String strAwayTeam) {
        this.strAwayTeam = strAwayTeam;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(String strDate) {
        this.dateEvent = strDate;
    }

    public String getStrHomeTeam() {
        return strHomeTeam;
    }

    public void setStrHomeTeam(String strHomeTeam) {
        this.strHomeTeam = strHomeTeam;
    }

}
