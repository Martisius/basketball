package com.example.lukasmartisius.basketball.Repositories.AllRepositories.EventRepository;

import android.arch.lifecycle.LiveData;

import com.example.lukasmartisius.basketball.Interfaces.Callback;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;

import java.util.List;

public interface IEventsManager {
    void getRequiredInfoList(int teamID, Callback<LiveData<List<EventDb>>> result);
}
