package com.example.lukasmartisius.basketball.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.support.annotation.Nullable;

import com.example.lukasmartisius.basketball.Interfaces.Callback;
import com.example.lukasmartisius.basketball.Interfaces.OnResultViewListener;
import com.example.lukasmartisius.basketball.Model.RoomModels.TeamDb;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository.ITeamsManager;

import java.util.List;

public class TeamViewModel extends BaseObservable  {
    private ITeamsManager teamsManager;
    public final ObservableField<Boolean> isLoading = new ObservableField<>();
    private LiveData<List<TeamDb>> teamList;
    OnResultViewListener onResultViewListener;

    public TeamViewModel(final OnResultViewListener onResultViewListener, ITeamsManager teamsManager) {
        this.teamsManager = teamsManager;
        this.onResultViewListener = onResultViewListener;
    }

    public void loadTeamList()
    {
            isLoading.set(true);
        teamsManager.getTeamList(new Callback<LiveData<List<TeamDb>>>(){
                @Override
                public void onResult(LiveData<List<TeamDb>> result)
                {
                    teamList = result;
                    isLoading.set(false);
                }

                @Override
                public void onFailure(Throwable result) {
                    isLoading.set(false);
                    onResultViewListener.OnFailure(result);
                }
            });
    }

    public LiveData<List<TeamDb>> getTeamList() {
        return teamList;
    }

    public boolean shouldRefresh()
    {
        return false;
    }
}
