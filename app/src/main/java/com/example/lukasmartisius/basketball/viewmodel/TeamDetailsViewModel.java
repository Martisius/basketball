package com.example.lukasmartisius.basketball.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import com.example.lukasmartisius.basketball.Interfaces.Callback;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.EventRepository.IEventsManager;
import com.example.lukasmartisius.basketball.Interfaces.OnResultViewListener;
import com.example.lukasmartisius.basketball.Model.NeededModels.Team;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;
import com.example.lukasmartisius.basketball.Model.RoomModels.PlayerDb;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository.IPlayersManager;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.TeamRepository.ITeamsManager;

import java.util.List;

public class TeamDetailsViewModel extends ViewModel {
    private ITeamsManager teamsManager;
    private IEventsManager eventsManager;
    private IPlayersManager playersManager;
    public ObservableField<Boolean> isLoading = new ObservableField<>(false);
    private Team team;
    private LiveData<List<PlayerDb>> playersList;
    private LiveData<List<EventDb>> infosList;
    private OnResultViewListener onResultViewListener;

    public void init(final OnResultViewListener onResultViewListener, ITeamsManager teamsManager, IEventsManager eventsManager, IPlayersManager playersManager) {
        this.teamsManager = teamsManager;
        this.eventsManager = eventsManager;
        this.playersManager = playersManager;
        this.onResultViewListener = onResultViewListener;
    }

    public void loadLists(final int teamID){
        isLoading.set(true);

        team = teamsManager.getTeam(teamID);
        eventsManager.getRequiredInfoList(teamID, new Callback<LiveData<List<EventDb>>>(){
                    @Override
                    public void onResult(LiveData<List<EventDb>> result) {
                        infosList = result;
                        playersManager.getPlayersInTeamList(teamID, new Callback<LiveData<List<PlayerDb>>>(){
                            @Override
                            public void onResult(LiveData<List<PlayerDb>> result) {
                                playersList = result;
                                isLoading.set(false);
                            }
                            @Override
                            public void onFailure(Throwable result){
                                isLoading.set(false);
                                onResultViewListener.OnFailure(result);
                            }
                        });
                    }
                    @Override
                    public void onFailure(Throwable result){
                        isLoading.set(false);
                        onResultViewListener.OnFailure(result);
                    }
                });
    }

    public LiveData<List<EventDb>> getEventList() {
        return infosList;
    }

    public LiveData<List<PlayerDb>> getPlayersList() {
        return playersList;
    }

    public String getTeamImage(){
        return team.getTeamImage();
    }

    public String getTeamName() {
        return team.getTeamName();
    }

    public int getTeamId(){
        return team.getID();
    }

}
