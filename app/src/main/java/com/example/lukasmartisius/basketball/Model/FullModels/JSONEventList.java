package com.example.lukasmartisius.basketball.Model.FullModels;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JSONEventList {
    @SerializedName("events")
    List<JSONEvent> eventList;

    public JSONEventList()
    {
        eventList = new ArrayList<JSONEvent>();
    }

    public static JSONEventList parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        JSONEventList events = gson.fromJson(response, JSONEventList.class);
        return events;
    }

    public List<JSONEvent> getEventList(){
        return eventList;
    }
}
