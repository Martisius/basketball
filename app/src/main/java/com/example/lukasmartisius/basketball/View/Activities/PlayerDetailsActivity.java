package com.example.lukasmartisius.basketball.View.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.example.lukasmartisius.basketball.Dagger.DaggerActivity;
import com.example.lukasmartisius.basketball.Interfaces.OnResultViewListener;
import com.example.lukasmartisius.basketball.R;
import com.example.lukasmartisius.basketball.Repositories.AllRepositories.PlayerRepository.IPlayersManager;
import com.example.lukasmartisius.basketball.databinding.ActivityPlayerDetailsBinding;
import com.example.lukasmartisius.basketball.viewmodel.PlayerViewModel;

import javax.inject.Inject;

public class PlayerDetailsActivity extends DaggerActivity implements OnResultViewListener {

    @Inject
    IPlayersManager playersManager;

    private final static String PLAYER_ID = "playerID";
    private ActivityPlayerDetailsBinding binding;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_player_details);
        Intent intent = getIntent();
        int playerID = intent.getIntExtra(PLAYER_ID, 0);
        binding.setViewModel(new PlayerViewModel(playerID, playersManager));
        binding.executePendingBindings();
    }
    public void onBackClick(View v)
    {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void OnFailure(Throwable t) {
        Snackbar.make(findViewById(android.R.id.content), "Loading failed", Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.RED)
                .show();
    }
}
