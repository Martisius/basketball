package com.example.lukasmartisius.basketball.UsableObjects.RefreshManager;

public interface IRefreshManager {
    void refreshDate(int ID, int teamID);
    boolean shouldRefresh(int ID, int teamID);
}
