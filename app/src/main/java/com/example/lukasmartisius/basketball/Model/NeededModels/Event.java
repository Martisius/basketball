package com.example.lukasmartisius.basketball.Model.NeededModels;

public class Event {
    private String aTeam;
    private String bTeam;
    private String date;

    public Event(String date, String myTeamName, String rivalTeamName)
    {
        this.aTeam = myTeamName;
        this.bTeam = rivalTeamName;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getbTeamName() {
        return bTeam;
    }

    public void setbTeamName(String bTeamName) {
        this.bTeam = bTeamName;
    }

    public String getaTeamName() {

        return aTeam;
    }

    public void setaTeamName(String aTeamName) {
        this.aTeam = aTeamName;
    }
}
