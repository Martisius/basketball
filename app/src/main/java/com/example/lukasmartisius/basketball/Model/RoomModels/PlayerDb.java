package com.example.lukasmartisius.basketball.Model.RoomModels;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.lukasmartisius.basketball.UsableObjects.DbContract;

@Entity(tableName = DbContract.DbEntry.PLAYER_TABLE_NAME)
public class PlayerDb {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_LOCAL_ID)
    private int localID;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_NAME)
    private String playerName;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_AGE)
    private int playerAge;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_HEIGHT)
    private String playerHeight;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_WEIGHT)
    private String playerWeight;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_DESCRIPTION)
    private String playerDescription;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_POSITION)
    private String playerPosition;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_IMAGE)
    private String playerImage;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_PLAYER_ID)
    private int playerID;

    @ColumnInfo(name = DbContract.DbEntry.COLUMN_NAME_TEAM_ID)
    private int teamID;

    public int getLocalID() {
        return localID;
    }

    public void setLocalID(int localID) {
        this.localID = localID;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getPlayerAge() {
        return playerAge;
    }

    public void setPlayerAge(int playerAge) {
        this.playerAge = playerAge;
    }

    public String getPlayerHeight() {
        return playerHeight;
    }

    public void setPlayerHeight(String playerHeight) {
        this.playerHeight = playerHeight;
    }

    public String getPlayerWeight() {
        return playerWeight;
    }

    public void setPlayerWeight(String playerWeight) {
        this.playerWeight = playerWeight;
    }

    public String getPlayerDescription() {
        return playerDescription;
    }

    public void setPlayerDescription(String playerDescription) {
        this.playerDescription = playerDescription;
    }

    public String getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition(String playerPosition) {
        this.playerPosition = playerPosition;
    }

    public String getPlayerImage() {
        return playerImage;
    }

    public void setPlayerImage(String playerImage) {
        this.playerImage = playerImage;
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public int getTeamID() {
        return teamID;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }
}
