package com.example.lukasmartisius.basketball.Dagger;

import android.app.Application;

import com.example.lukasmartisius.basketball.AppModule;
import com.example.lukasmartisius.basketball.NBAApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, ActivityBuilder.class, DaggerActivity.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();

    }

    void inject(NBAApplication app);

}
