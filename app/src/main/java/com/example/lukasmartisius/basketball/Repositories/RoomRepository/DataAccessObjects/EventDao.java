package com.example.lukasmartisius.basketball.Repositories.RoomRepository.DataAccessObjects;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;
import com.example.lukasmartisius.basketball.UsableObjects.DbContract;

import java.util.List;

@Dao
public interface EventDao {
    @Query("SELECT * FROM " + DbContract.DbEntry.EVENT_TABLE_NAME + " WHERE "
            + DbContract.DbEntry.COLUMN_NAME_HOME_TEAM_ID + " = :teamID OR "
            + DbContract.DbEntry.COLUMN_NAME_AWAY_TEAM_ID + " = :teamID")
    LiveData<List<EventDb>> getAll(int teamID);

    @Query("DELETE FROM " + DbContract.DbEntry.EVENT_TABLE_NAME + " WHERE "
            + DbContract.DbEntry.COLUMN_NAME_HOME_TEAM_ID + " = :teamID OR "
            + DbContract.DbEntry.COLUMN_NAME_AWAY_TEAM_ID + " = :teamID")
    void cleanEvents(int teamID);

    @Insert
    void insertAll(List<EventDb> events);

    @Insert
    void insert(EventDb... events);

    @Delete
    void delete(EventDb events);
}
