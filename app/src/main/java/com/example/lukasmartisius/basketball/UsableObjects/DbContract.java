package com.example.lukasmartisius.basketball.UsableObjects;

import android.provider.BaseColumns;

public class DbContract {

    private DbContract(){

    }

    public static class DbEntry implements BaseColumns {
        public static final String TEAM_TABLE_NAME = "TEAMS_TABLE";
        public static final String EVENT_TABLE_NAME = "EVENTS_TABLE";
        public static final String PLAYER_TABLE_NAME = "PLAYERS_TABLE";

        //team table
        public static final String COLUMN_NAME_TEAM_LOCAL_ID = "ID";
        public static final String COLUMN_NAME_TEAM_NAME = "TEAM_NAME";
        public static final String COLUMN_NAME_TEAM_DESCRIPTION = "TEAM_DESCRIPTION";
        public static final String COLUMN_NAME_TEAM_ICON = "TEAM_ICON";
        public static final String COLUMN_NAME_TEAM_IMAGE = "TEAM_IMAGE";
        public static final String COLUMN_NAME_TEAM_ID = "TEAM_ID";

        //event table
        public static final String COLUMN_NAME_EVENT_LOCAL_ID = "ID";
        public static final String COLUMN_NAME_EVENT_DATE = "EVENT_DATE";
        public static final String COLUMN_NAME_HOME_TEAM = "HOME_TEAM";
        public static final String COLUMN_NAME_AWAY_TEAM = "AWAY_TEAM";
        public static final String COLUMN_NAME_HOME_TEAM_ID = "HOME_TEAM_ID";
        public static final String COLUMN_NAME_AWAY_TEAM_ID = "AWAY_TEAM_ID";

        //player table
        public static final String COLUMN_NAME_PLAYER_LOCAL_ID = "ID";
        public static final String COLUMN_NAME_PLAYER_NAME = "PLAYER_NAME";
        public static final String COLUMN_NAME_PLAYER_AGE = "PLAYER_AGE";
        public static final String COLUMN_NAME_PLAYER_HEIGHT = "PLAYER_HEIGHT";
        public static final String COLUMN_NAME_PLAYER_WEIGHT = "PLAYER_WEIGHT";
        public static final String COLUMN_NAME_PLAYER_DESCRIPTION = "PLAYER_DESCRIPTION";
        public static final String COLUMN_NAME_PLAYER_POSITION = "PLAYER_POSITION";
        public static final String COLUMN_NAME_PLAYER_IMAGE = "PLAYER_IMAGE";
        public static final String COLUMN_NAME_PLAYER_ID = "PLAYER_ID";


        public static final String SQL_CREATE_TEAM_ENTRIES =
                "CREATE TABLE " + TEAM_TABLE_NAME + " (" +
                        COLUMN_NAME_TEAM_LOCAL_ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME_TEAM_NAME + " varchar(255)," +
                        COLUMN_NAME_TEAM_DESCRIPTION + " TEXT," +
                        COLUMN_NAME_TEAM_ICON + " TEXT," +
                        COLUMN_NAME_TEAM_IMAGE + " TEXT," +
                        COLUMN_NAME_TEAM_ID + " int)";

        public static final String SQL_DELETE_TEAM_ENTRIES =
                "DROP TABLE IF EXISTS " + TEAM_TABLE_NAME;

        public static final String SQL_CREATE_EVENT_ENTRIES =
                "CREATE TABLE " + EVENT_TABLE_NAME + " (" +
                        COLUMN_NAME_EVENT_LOCAL_ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME_EVENT_DATE + " varchar(255)," +
                        COLUMN_NAME_HOME_TEAM + " TEXT," +
                        COLUMN_NAME_AWAY_TEAM + " TEXT," +
                        COLUMN_NAME_HOME_TEAM_ID + " int," +
                        COLUMN_NAME_AWAY_TEAM_ID + " int)";

        public static final String SQL_DELETE_EVENT_ENTRIES =
                "DROP TABLE IF EXISTS " + EVENT_TABLE_NAME;

        public static final String SQL_CREATE_PLAYER_ENTRIES =
                "CREATE TABLE " + PLAYER_TABLE_NAME + " (" +
                        COLUMN_NAME_PLAYER_LOCAL_ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME_PLAYER_NAME + " varchar(255)," +
                        COLUMN_NAME_PLAYER_AGE + " varchar(255)," +
                        COLUMN_NAME_PLAYER_HEIGHT + " varchar(255)," +
                        COLUMN_NAME_PLAYER_WEIGHT + " varchar(255)," +
                        COLUMN_NAME_PLAYER_DESCRIPTION + " TEXT," +
                        COLUMN_NAME_PLAYER_POSITION + " varchar(255)," +
                        COLUMN_NAME_PLAYER_IMAGE + " TEXT," +
                        COLUMN_NAME_PLAYER_ID + " int," +
                        COLUMN_NAME_TEAM_ID + " int, " +
                        "FOREIGN KEY(" + COLUMN_NAME_TEAM_ID + ") REFERENCES " + TEAM_TABLE_NAME + "(" + COLUMN_NAME_TEAM_ID + "))";

        public static final String SQL_DELETE_PLAYER_ENTRIES =
                "DROP TABLE IF EXISTS " + PLAYER_TABLE_NAME;
    }
}
