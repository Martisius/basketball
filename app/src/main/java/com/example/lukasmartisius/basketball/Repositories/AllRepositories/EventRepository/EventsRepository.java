package com.example.lukasmartisius.basketball.Repositories.AllRepositories.EventRepository;

import android.arch.lifecycle.LiveData;

import com.example.lukasmartisius.basketball.Interfaces.Callback;
import com.example.lukasmartisius.basketball.Model.FullModels.JSONEventList;
import com.example.lukasmartisius.basketball.Model.RoomModels.EventDb;
import com.example.lukasmartisius.basketball.Repositories.RoomRepository.IDatabaseManager;
import com.example.lukasmartisius.basketball.RetrofitApi.IRetrofitManager;
import com.example.lukasmartisius.basketball.UsableObjects.Converters.Single.EventConverter.IEventMapper;
import com.example.lukasmartisius.basketball.UsableObjects.RefreshManager.IRefreshManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class EventsRepository implements IEventsManager {

    private IRefreshManager refreshManager;
    private IRetrofitManager retrofitApi;
    private IDatabaseManager database;
    private IEventMapper eventMapper;

    public EventsRepository(IRefreshManager refreshManager, IRetrofitManager retrofitApi, IDatabaseManager database, IEventMapper eventMapper) {
        this.refreshManager = refreshManager;
        this.retrofitApi = retrofitApi;
        this.database = database;
        this.eventMapper = eventMapper;
    }

    @Override
    public void getRequiredInfoList(final int teamID, final Callback<LiveData<List<EventDb>>> result) {
        if(refreshManager.shouldRefresh(2, teamID)) {
            final Call<JSONEventList> repos = retrofitApi.getEventList(teamID);
            repos.enqueue(new retrofit2.Callback<JSONEventList>() {
                @Override
                public void onResponse(Call<JSONEventList> call, final Response<JSONEventList> response) {
                    JSONEventList jsonEventList = response.body();
                    List<EventDb> eventDbs = eventMapper.mapFromJsonToDb(jsonEventList);
                    database.updateEvents(teamID, eventDbs);
                    refreshManager.refreshDate(2, teamID);
                    result.onResult(database.getEventsList(teamID));
                }

                @Override
                public void onFailure(Call<JSONEventList> call, Throwable t) {
                    result.onFailure(t);
                }
            });
        } else {
            result.onResult(database.getEventsList(teamID));
        }
    }
}
