package com.example.lukasmartisius.basketball.UsableObjects.RefreshManager;

import android.content.SharedPreferences;

import com.example.lukasmartisius.basketball.Enums.UpdateTime;
import com.example.lukasmartisius.basketball.UsableObjects.LastUpdate;
import com.google.gson.Gson;

public class RefreshManagerImpl implements IRefreshManager {
    private static final LastUpdate ZERO_TIME = new LastUpdate(0, 0L, 0L, 0L);
    private final SharedPreferences prefs;
    private static final Gson gson = new Gson();

    private static final int TEAM = 1;
    private static final int EVENT = 2;
    private static final int PLAYER = 3;


    public RefreshManagerImpl(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    @Override
    public void refreshDate(int ID, int teamID) {
        SharedPreferences.Editor editor = prefs.edit();

        if(ID == TEAM) {
            editor.remove(Integer.toString(ID));
            editor.putLong(Integer.toString(ID), System.currentTimeMillis());
        } else {
            LastUpdate lastUpdate = gson.fromJson(prefs.getString(Integer.toString(teamID), gson.toJson(ZERO_TIME)), LastUpdate.class);
            editor.remove(Integer.toString(teamID));
            if(ID == EVENT)
                lastUpdate.setTeamNewsUpdateStamp(System.currentTimeMillis());
            if(ID == PLAYER)
                lastUpdate.setPlayerUpdateTimeStamp(System.currentTimeMillis());
            editor.putString(Integer.toString(teamID), gson.toJson(lastUpdate));
        }
        editor.apply();
    }

    @Override
    public boolean shouldRefresh(int ID, int teamID) {
        LastUpdate lastUpdate = gson.fromJson(prefs.getString(Integer.toString(teamID), gson.toJson(ZERO_TIME)), LastUpdate.class);
        if(ID == TEAM)
            return (System.currentTimeMillis()-UpdateTime.getById(ID).getRenewalRate()>prefs.getLong(Integer.toString(ID), 0L));
        if(ID == EVENT)
            return (System.currentTimeMillis()-UpdateTime.getById(ID).getRenewalRate()>lastUpdate.getTeamNewsUpdateStamp());
        if(ID == PLAYER)
            return (System.currentTimeMillis()-UpdateTime.getById(ID).getRenewalRate()>lastUpdate.getPlayerUpdateTimeStamp());

        return false;
    }
}
